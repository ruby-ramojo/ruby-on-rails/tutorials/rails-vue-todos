class TodosController < ApplicationController
    
    # set_todo will only be called for the methods specified here
    before_action :set_todo, only: [:show, :update, :destroy] 

    def index
        @todos = Todo.all
        render json: @todos
    end

    def show
        render json: @todo
    end

    
    def create
        @todo = Todo.new(todo_params)
        if @todo.save
            render json: @todo
        else
            render json: @todo.errors, status: :unprocessable_entity
        end
    end

    def update
        if @todo.update(todo_params)
            render json: @todo
        else
            render json: @todo.errors, status: :unprocessable_entity
        end
    end

    def destroy
        @todo.destroy
    end

    
    private
        def set_todo
            @todo = Todo.find(params[:id])
            # The fact that it is declared here allows it to be called in any of the methods here without having to declare it constantly e.g. in the show method
        end

        def todo_params
            params.require(:todo).permit(:id, :title, :completed)
        end
end
